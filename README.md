# Thinktecture IdentityServer  STS Relying Party application in PHP#
### Description.
I needed a php Relying Party (RP) application that will connect to Thinktecture IdentityServer sercurity token service (STS)
SimpleSAML library has WSFederation implementation that would use SAML1 token, but in my case token bearer was SAML2 and the token was encrypted by the STS.
SimpleSAML has also SAML2 protocol implementation with SAML2 token bearer, but it doesn't support WS-Trust. It also has SAML2 token decryption using XmlSecLibs
So I needed to put the pieces together and create this php RP.

### 1. Sorting out the certificates.

1.1. Signing certificate.
I used Base-64 X.509 public key of the STS signing certificate and saved it to _cert/encryption.cer.
In .NET relying party using Windows Identity Foundation (WIF) that certificate is configured in the "trustedIssuers" section.

1.2. Encryption certificate
This is the private key of the certificate which the STS uses to encrypt the SAML2 token.
I had the EncryptionCertificateWithPrivateKey.pfx file and used openssl to get only the private key and save it to _cert/key.pem
    openssl pkcs12 -in EncryptionCertificateWithPrivateKey.pfx -nocerts -out privateKey.pem
    openssl rsa -in privateKey.pem -out key.pem
In .NET relying party using Windows Identity Foundation (WIF) that certificate is configured in the "serviceCertificate" section.

### 2. Server configuration.

I needed to compile php with openssl and mcrypt. I also setup self signed certificate for apache ssl setup, because the STS was requiring https for the relying parties. Here are the versions I used:

    Apache/2.2.14 (Unix) PHP/5.3.24 mod_ssl/2.2.22 OpenSSL/0.9.8r mod_fastcgi/2.4.2
    mod_apreq2-20090110/2.7.1 mod_perl/2.0.4 Perl/v5.10.0
    PHP compiled with mcrypt 2.5.8 and OpenSSL 0.9.8r
    

### 3. STS configuration.

The STS had already the signing certificate configured so I added relying party 'https://localhost/' and added the public key of the encryption certificate (see 1.2).

### 4. RP implementation.

_lib/WSFed.php file contains the implementation of the WS-Federation protocol and handles the singin/signout. It also uses helper class Identity defined in _lib/Identity.php to store the claims from the token into the session.
_lib/WSFedResponse.php contains methods for parsing the response from the STS including decrypting the SAML2 token.
The configuration is in index.php
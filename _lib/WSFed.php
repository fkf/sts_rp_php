<?
// process logout
if (!empty($_GET['wa']) and ($_GET['wa'] == 'wsignoutcleanup1.0')) {
     unset($_SESSION['identity']);
	print 'Logged Out';
	exit;
}

// process token
if (!empty($_POST['wa']) and ($_POST['wa'] == 'wsignin1.0') and !empty($_POST['wresult'])) {
    try {
        $wa = $_POST['wa'];
        $wresult = $_POST['wresult'];
        $wctx = $_POST['wctx'];

        /* Accommodate for MS-ADFS escaped quotes */
        $wresult = str_replace('\"', '"', $wresult);
        $wresult = str_replace ("\r", "", $wresult);

        $samlResponse = new WSFed_Response($wresult, readcert($certPath), readcert($keyPath));

        if ($samlResponse->isValid()) {
            $_SESSION['identity'] = $samlResponse->getAttributes();
        }
        else {
            echo 'Invalid SAML response.';
        }
    } catch (Exception $exception) {
        echo $exception;
    }
}

// process login
try {
    if (empty($_SESSION['identity'])) {
        redirect($sts, array(
            'wa' => 'wsignin1.0',
            'wct' =>  gmdate('Y-m-d\TH:i:s\Z', time()),
            'wtrealm' => $audienceUrl,
            'wctx' => $audienceUrl
            ));
	}
} catch (Exception $exception) {
	echo $exception;
}

$identity = new Identity($_SESSION['identity']);

// helper functions

function redirect($url, $parameters) {
    if(strpos($url, '?') === FALSE) {
		$paramPrefix = '?';
	} else {
		$paramPrefix = '&';
	}
    foreach($parameters as $name => $value) {
        /* Encode the parameter. */
        if($value === NULL) {
            $param = urlencode($name);
        } elseif (is_array($value)) {
            $param = "";
            foreach ($value as $val) {
                $param .= urlencode($name) . "[]=" . urlencode($val) . '&';
            }
        } else {
            $param = urlencode($name) . '=' .
                urlencode($value);
        }

        /* Append the parameter to the query string. */
        $url .= $paramPrefix . $param;

        /* Every following parameter is guaranteed to follow
         * another parameter. Therefore we use the '&' prefix.
         */
        $paramPrefix = '&';
    }


    /* Set the HTTP result code. This is either 303 See Other or
     * 302 Found. HTTP 303 See Other is sent if the HTTP version
     * is HTTP/1.1 and the request type was a POST request.
     */
    if($_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' &&
        $_SERVER['REQUEST_METHOD'] === 'POST') {
        $code = 303;
    } else {
        $code = 302;
    }

    /* Set the location header. */
    header('Location: ' . $url, TRUE, $code);

    /* Disable caching of this response. */
    header('Pragma: no-cache');
    header('Cache-Control: no-cache, must-revalidate');

    /* Show a minimal web page with a clickable link to the URL. */
    echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"' .
        ' "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' . "\n";
    echo '<html xmlns="http://www.w3.org/1999/xhtml">';
    echo '<head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8">
                <title>Redirect</title>
            </head>';
    echo '<body>';
    echo '<h1>Redirect</h1>';
    echo '<p>';
    echo 'You were redirected to: ';
    echo '<a id="redirlink" href="' . htmlspecialchars($url) . '">' . htmlspecialchars($url) . '</a>';
    echo '<script type="text/javascript">document.getElementById("redirlink").focus();</script>';
    echo '</p>';
    echo '</body>';
    echo '</html>';

    /* End script execution. */
    exit;
}

function readcert($certPath) {
    $fp = fopen($certPath, "r");
    $cert = fread($fp, 8192);
    fclose($fp);
    return $cert;
}
?>
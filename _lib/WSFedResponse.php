<?php

/**
 * Parse the WSTrust response. Decrypt token if needed
 */
class WSFed_Response
{
    protected $_cert;
    protected $_document;
    public $xml;

    public function __construct($xmlResponse, $cert, $pkey)
    {
        $this->_cert = $cert;
        $this->xml = $xmlResponse;
        $this->_document = new DOMDocument();
        $this->_document->loadXML($this->xml);

        //decrypt data
        $encryptedAssertion = $this->_queryEncryptedAssertion('');
        if ($encryptedAssertion) {
        	$inputKey = new XMLSecurityKey(XMLSecurityKey::RSA_OAEP_MGF1P, array('type'=>'private'));
        	//$inputKey = new XMLSecurityKey(XMLSecurityKey::AES256_CBC, array('type'=>'private'));
			$inputKey->loadKey($pkey);

        	$decrypted = $this->_decryptElement($this->_queryEncryptedAssertion('/xenc:EncryptedData'), $inputKey);

        	$this->xml = preg_replace('/<EncryptedAssertion.+?EncryptedAssertion>/', $decrypted, $this->xml);
        	$this->_document = new DOMDocument();
            $this->_document->loadXML($this->xml);
        }
    }

    private function validateNumAssertions()
    {
        $assertionNodes = $this->_document->getElementsByTagName('Assertion');
        return ($assertionNodes->length == 1);
    }

    /**
     * Verify that the document is still valid according
     *
     * @return bool
     */
    private function validateTimestamps()
    {
        $timestampNodes = $this->_document->getElementsByTagName('Conditions');
        for ($i = 0; $i < $timestampNodes->length; $i++) {
            $nbAttribute = $timestampNodes->item($i)->attributes->getNamedItem("NotBefore");
            $naAttribute = $timestampNodes->item($i)->attributes->getNamedItem("NotOnOrAfter");
            if ($nbAttribute && strtotime($nbAttribute->textContent) > time()) {
                return false;
            }
            if ($naAttribute && strtotime($naAttribute->textContent) <= time()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isValid()
    {
        $objXMLSecDSig = new XMLSecurityDSig();

        $objDSig = $objXMLSecDSig->locateSignature(clone $this->_document);
        if (!$objDSig) {
            throw new Exception('Cannot locate Signature Node');
        }
        $objXMLSecDSig->canonicalizeSignedInfo();
        $objXMLSecDSig->idKeys = array('ID');

        $retVal = $objXMLSecDSig->validateReference();
        if (!$retVal) {
            throw new Exception('Reference Validation Failed');
        }

        $singleAssertion = $this->validateNumAssertions();
        if (!$singleAssertion) {
            throw new Exception('Multiple assertions are not supported');
        }

        $validTimestamps = $this->validateTimestamps();
        if (!$validTimestamps) {
            throw new Exception('Timing issues (please check your clock settings)
            ');
        }

        $objKey = $objXMLSecDSig->locateKey();
        if (!$objKey) {
            throw new Exception('We have no idea about the key');
        }

        XMLSecEnc::staticLocateKeyInfo($objKey, $objDSig);

        $objKey->loadKey($this->_cert, FALSE, TRUE);

        return ($objXMLSecDSig->verify($objKey) === 1);
    }

    /**
     * Get the NameID provided by the SAML response from the IdP.
     */
    public function getNameId()
    {
        $entries = $this->_queryAssertion('/saml:Subject/saml:NameID');
        return $entries->item(0)->nodeValue;
    }

    public function getAudienceUrl()
    {
        $entries = $this->_queryAssertion('/saml:Subject/saml:NameID');
        return $entries->item(0)->nodeValue;
    }

    /**
     * Get the SessionNotOnOrAfter attribute, as Unix Epoc, from the
     * AuthnStatement element.
     * Using this attribute, the IdP suggests the local session expiration
     * time.
     *
     * @return The SessionNotOnOrAfter as unix epoc or NULL if not present
     */
    public function getSessionNotOnOrAfter()
    {
        $entries = $this->_queryAssertion('/saml:AuthnStatement[@SessionNotOnOrAfter]');
        if ($entries->length == 0) {
            return NULL;
        }
        $notOnOrAfter = $entries->item(0)->getAttribute('SessionNotOnOrAfter');
        return strtotime($notOnOrAfter);
    }

    public function getAttributes()
    {
        $entries = $this->_queryAssertion('/saml:AttributeStatement/saml:Attribute');

        $attributes = array();
        /** @var $entry DOMNode */
        foreach ($entries as $entry) {
            $attributeName = $entry->attributes->getNamedItem('Name')->nodeValue;

            $attributeValues = array();
            foreach ($entry->childNodes as $childNode) {
                if ($childNode->nodeType == XML_ELEMENT_NODE && $childNode->tagName === 'AttributeValue'){
                    $attributeValues[] = $childNode->nodeValue;
                }
            }

            $attributes[$attributeName] = $attributeValues;
        }
        return $attributes;
    }

    /**
     * @param string $assertionXpath
     * @return DOMNodeList
     */
    protected function _queryEncryptedAssertion($queryPath)
    {
        $xpath = new DOMXPath($this->_document);
        $xpath->registerNamespace('trust'    , "http://docs.oasis-open.org/ws-sx/ws-trust/200512");
        $xpath->registerNamespace('saml'    , 'urn:oasis:names:tc:SAML:2.0:assertion');
        $xpath->registerNamespace('ds'      , 'http://www.w3.org/2000/09/xmldsig#');
        $xpath->registerNamespace('xenc'    , 'http://www.w3.org/2001/04/xmlenc#');
        $wstrust = '/trust:RequestSecurityTokenResponseCollection/trust:RequestSecurityTokenResponse/trust:RequestedSecurityToken';
        return $xpath->query("$wstrust/saml:EncryptedAssertion".$queryPath)->item(0);
    }

    /**
     * @param string $assertionXpath
     * @return DOMNodeList
     */
    protected function _queryAssertion($assertionXpath)
    {
        $xpath = new DOMXPath($this->_document);
        $xpath->registerNamespace('trust'    , "http://docs.oasis-open.org/ws-sx/ws-trust/200512");
        $xpath->registerNamespace('saml'    , 'urn:oasis:names:tc:SAML:2.0:assertion');
        $xpath->registerNamespace('ds'      , 'http://www.w3.org/2000/09/xmldsig#');
        $wstrust = '/trust:RequestSecurityTokenResponseCollection/trust:RequestSecurityTokenResponse/trust:RequestedSecurityToken';
        $signatureQuery = "$wstrust/saml:Assertion/ds:Signature/ds:SignedInfo/ds:Reference";
        $assertionReferenceNode = $xpath->query($signatureQuery)->item(0);
        if (!$assertionReferenceNode) {
            throw new Exception('Unable to query assertion, no Signature Reference found?');
        }
        $id = substr($assertionReferenceNode->attributes->getNamedItem('URI')->nodeValue, 1);
        $nameQuery = "$wstrust/saml:Assertion[@ID='$id']" . $assertionXpath;
        return $xpath->query($nameQuery);
    }

    	/**
	 * Decrypt an encrypted element.
	 *
	 * This is an internal helper function.
	 *
	 * @param DOMElement $encryptedData  The encrypted data.
	 * @param XMLSecurityKey $inputKey  The decryption key.
	 * @return DOMElement  The decrypted element.
	 */
	protected function _decryptElement(DOMElement $encryptedData, XMLSecurityKey $inputKey) {

		$enc = new XMLSecEnc();

		$enc->setNode($encryptedData);
		$enc->type = $encryptedData->getAttribute("Type");

		$symmetricKey = $enc->locateKey($encryptedData);
		if (!$symmetricKey) {
			throw new Exception('Could not locate key algorithm in encrypted data.');
		}

		$symmetricKeyInfo = $enc->locateKeyInfo($symmetricKey);
		if (!$symmetricKeyInfo) {
			throw new Exception('Could not locate <dsig:KeyInfo> for the encrypted key.');
		}

		$inputKeyAlgo = $inputKey->getAlgorith();
		if ($symmetricKeyInfo->isEncrypted) {
			$symKeyInfoAlgo = $symmetricKeyInfo->getAlgorith();

			if ($symKeyInfoAlgo === XMLSecurityKey::RSA_OAEP_MGF1P && $inputKeyAlgo === XMLSecurityKey::RSA_1_5) {
				/*
				 * The RSA key formats are equal, so loading an RSA_1_5 key
				 * into an RSA_OAEP_MGF1P key can be done without problems.
				 * We therefore pretend that the input key is an
				 * RSA_OAEP_MGF1P key.
				 */
				$inputKeyAlgo = XMLSecurityKey::RSA_OAEP_MGF1P;
			}

			/* Make sure that the input key format is the same as the one used to encrypt the key. */
			if ($inputKeyAlgo !== $symKeyInfoAlgo) {
				throw new Exception('Algorithm mismatch between input key and key used to encrypt ' .
					' the symmetric key for the message. Key was: ' .
					var_export($inputKeyAlgo, TRUE) . '; message was: ' .
					var_export($symKeyInfoAlgo, TRUE));
			}

			$encKey = $symmetricKeyInfo->encryptedCtx;
			$symmetricKeyInfo->key = $inputKey->key;

			$keySize = $symmetricKey->getSymmetricKeySize();
			if ($keySize === NULL) {
				/* To protect against "key oracle" attacks, we need to be able to create a
				 * symmetric key, and for that we need to know the key size.
				 */
				throw new Exception('Unknown key size for encryption algorithm: ' . var_export($symmetricKey->type, TRUE));
			}

			try {
				$key = $encKey->decryptKey($symmetricKeyInfo);
				if (strlen($key) != $keySize) {
					throw new Exception('Unexpected key size (' . strlen($key) * 8 . 'bits) for encryption algorithm: ' .
										var_export($symmetricKey->type, TRUE));
				}
			} catch (Exception $e) {
				/* We failed to decrypt this key. Log it, and substitute a "random" key. */
				SimpleSAML_Logger::error('Failed to decrypt symmetric key: ' . $e->getMessage());
				/* Create a replacement key, so that it looks like we fail in the same way as if the key was correctly padded. */

				/* We base the symmetric key on the encrypted key and private key, so that we always behave the
				 * same way for a given input key.
				 */
				$encryptedKey = $encKey->getCipherValue();
				$pkey = openssl_pkey_get_details($symmetricKeyInfo->key);
				$pkey = sha1(serialize($pkey), TRUE);
				$key = sha1($encryptedKey . $pkey, TRUE);

				/* Make sure that the key has the correct length. */
				if (strlen($key) > $keySize) {
					$key = substr($key, 0, $keySize);
				} elseif (strlen($key) < $keySize) {
					$key = str_pad($key, $keySize);
				}
			}
			$symmetricKey->loadkey($key);

		} else {
			$symKeyAlgo = $symmetricKey->getAlgorith();
			/* Make sure that the input key has the correct format. */
			if ($inputKeyAlgo !== $symKeyAlgo) {
				throw new Exception('Algorithm mismatch between input key and key in message. ' .
					'Key was: ' . var_export($inputKeyAlgo, TRUE) . '; message was: ' .
					var_export($symKeyAlgo, TRUE));
			}
			$symmetricKey = $inputKey;
		}

		$algorithm = $symmetricKey->getAlgorith();
		return $enc->decryptNode($symmetricKey, FALSE);
	}

}

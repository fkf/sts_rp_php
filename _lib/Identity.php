<?php
class Identity
{
    public $claims;
    public $namespace = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/';

    public function __construct($claims)
    {
        $this->claims = $claims;
    }

    public function getClaimValues($claimName)
    {
        return $this->claims[$this->namespace . $claimName];
    }

    public function getClaimValue($claimName)
    {
        return $this->claims[$this->namespace . $claimName][0];
    }

    public function getPersonId()
    {
        return $this->getClaimValue('personid');
    }

    public function getEmail()
    {
        return $this->getClaimValue('email');
    }

    public function getFirstName()
    {
        return $this->getClaimValue('givenname');
    }

    public function getLastName()
    {
        return $this->getClaimValue('surname');
    }

    public function getUsername()
    {
        return $this->getClaimValue('username');
    }

}

<?php
date_default_timezone_set('Europe/Oslo');
session_start();

/* sts config begin */
$audienceUrl = 'https://localhost/';
$sts = 'https://yourstsurl.com/issue/wsfed';
$certPath = "./_cert/signing.cer";
$keyPath = "./_cert/key.pem";
/* sts config end */

require_once('./_lib/Identity.php');
require_once('./_lib/XmlSecLibs.php');
require_once('./_lib/WSFedResponse.php');
require_once('./_lib/WSFed.php'); // handle authentication. user identity stored in session

echo "<a href='".$sts."?wa=wsignout1.0&wreply=".urlencode($audienceUrl)."'>logout</a><br/><br/><br/>";

echo 'personId '.$identity->getPersonId().'<br/>';
echo 'email '.$identity->getEmail().'<br/>';
echo 'username '.$identity->getUsername().'<br/>';
echo 'fistname '.$identity->getFirstName().'<br/>';
echo 'lastname '.$identity->getLastName().'<br/>';

?>
